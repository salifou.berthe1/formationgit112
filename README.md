Arcueil (prononcé /aʁ.kœj/) est une commune française située dans le département du Val-de-Marne en région Île-de-France, faisant partie de la métropole du Grand Paris et du Forum métropolitain du Grand Paris.

Cette commune urbanisée à plus de 90 % disposait en 2010 de 45 % de logements sociaux1, de quelques espaces verts, et se situe en grande partie dans la vallée de la Bièvre. Son urbanisation date de plusieurs époques successives, ce qui se traduit par une juxtaposition de bâtiments de divers styles (brique, maison de maître, immeuble en béton), de bâti neuf et ancien, de friches industrielles et de bâtiments à l'abandon actuellement.

Démographie
Évolution démographique
Articles connexes : Histoire du recensement de la population en France et Démographie de la France.
L'évolution du nombre d'habitants est connue à travers les recensements de la population effectués dans la commune depuis 1793. Pour les communes de plus de 10 000 habitants les recensements ont lieu chaque année à la suite d'une enquête par sondage auprès d'un échantillon d'adresses représentant 8 % de leurs logements, contrairement aux autres communes qui ont un recensement réel tous les cinq ans48,Note 5.

En 2020, la commune comptait 21 840 habitantsNote 6, en augmentation de 4,44 % par rapport à 2014 (Val-de-Marne : +3,15 %, France hors Mayotte : +1,9 %).

Évolution de la population  [ modifier ]
1793	1800	1806	1821	1831	1836	1841	1846	1851
1 338	1 168	1 200	1 439	1 809	1 746	1 734	2 701	3 071
Évolution de la population  [ modifier ], suite (1)
1856	1861	1866	1872	1876	1881	1886	1891	1896
2 957	4 078	5 024	5 258	5 299	6 067	6 465	6 088	7 064
Évolution de la population  [ modifier ], suite (2)
1901	1906	1911	1921	1926	1931	1936	1946	1954
8 425	9 237	11 319	14 966	12 559	16 200	16 590	16 340	18 067
Évolution de la population  [ modifier ], suite (3)
1962	1968	1975	1982	1990	1999	2006	2011	2016
20 224	21 877	20 330	20 064	20 334	18 061	19 129	20 100	21 567
Évolution de la population  [ modifier ], suite (4)
2020	-	-	-	-	-	-	-	-
21 840	-	-	-	-	-	-	-	-
De 1962 à 1999 : population sans doubles comptes ; pour les dates suivantes : population municipale.
(Sources : Ldh/EHESS/Cassini jusqu'en 199931 puis Insee à partir de 200649.)
Histogramme de l'évolution démographique

Pyramide des âges
La population de la commune est relativement jeune. En 2020, le taux de personnes d'un âge inférieur à 30 ans s'élève à 40,9 %, soit au-dessus de la moyenne départementale (39,4 %). À l'inverse, le taux de personnes d'âge supérieur à 60 ans est de 18,4 % la même année, alors qu'il est de 20,2 % au niveau départemental.

En 2020, la commune comptait 10 583 hommes pour 11 257 femmes, soit un taux de 51,54 % de femmes, légèrement inférieur au taux départemental (51,85 %).

Les pyramides des âges de la commune et du département s'établissent comme suit.

Pyramide des âges de la commune en 2020 en pourcentage50
Hommes	Classe d’âge	Femmes
0,4 	
90 ou +
1,1 
4,3 	
75-89 ans
6,6 
11,3 	
60-74 ans
13,1 
20 	
45-59 ans
19,4 
20,5 	
30-44 ans
21,5 
24 	
15-29 ans
21,8 
19,5 	
0-14 ans
16,5 
Pyramide des âges du département du Val-de-Marne en 2020 en pourcentage51
Hommes	Classe d’âge	Femmes
0,5 	
90 ou +
1,4 
5 	
75-89 ans
7,2 
12,6 	
60-74 ans
13,5 
19,5 	
45-59 ans
19,2 
21,2 	
30-44 ans
21,1 
20,7 	
15-29 ans
19,3 
20,5 	
0-14 ans
18,3 

Toponymie
Panneau à l'entrée de la commune.

Les formes anciennes de la localité sont :

    en latin in pago Arcolei v.107317, molendini apud Arcoilum v. 111017, molendinum de Arcoilo 111917, in villis de Arcolio, Archoilus 111918, Arcoilo 113618, Arcoilus 114218, ad villam de Arcolio 128319, territoriis de Arcolio 130919, dans une ode du poète Donat ad fontem Arculi 158620.
    en français, en la ville d'Arcuel 126521, Arcueil 175722.

L'étymologie d'Arcueil est issu d'un mot gallo-romain composé du latin arcus « arche » + suff. gaulois -ialo « village, domaine », d'où « le domaine des arches », en rapport avec l'aqueduc23 encore visible sur les plans de 1757, Aqueduc Cachan22.

En 1894, le lieu-dit Cachan de la commune d'Arcueil avait pris une telle importance que son nom fut rattaché à celui d'Arcueil, la commune s'appelant désormais Arcueil-Cachan. Vingt-huit ans plus tard, en 1922, Cachan devint une commune indépendante et Arcueil reprit son nom d'origine. 
